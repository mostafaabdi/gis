from django.shortcuts import render
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse
from django.template.loader import get_template
from django.template.context import RequestContext
import geopandas as gpd

import folium
from folium.plugins import Draw
from folium.plugins import Search
from folium import FeatureGroup, LayerControl, Map, Marker
from folium.plugins import MiniMap
from .models import manabeModel
from folium.plugins import MousePosition

def show(request):
    # landuse = gpd.read_file("/home/milad/Desktop/m/iran_landuse.shp")
    # iran_location=gpd.read_file("/home/milad/Desktop/m/iran_location.shp")
    # landuse=gpd.read_file("/home/milad/Desktop/m/iran_landuse.shp")
    
    
    m=folium.Map(location=[33.888087,48.8237601],zoom_start=8,control_scale=True)

    # folium.GeoJson(landuse,name='کاربری زمین').add_to(m)
    # folium.GeoJson(iran_location,name="مکان ها").add_to(m)

    feature_group = FeatureGroup(name='اطلاعات کاربران')
    html = """
    <h1> این یک مکان ثابت است</h1><br>
این یک مکان ثابت است که سیستم به نقشه اضافه کرده است    <p>
    <code>
        بسیار زیبا است
    </code>
    </p>
    """
    Marker(location=[33.888087,48.8237601],
           popup=html,parse_html=True,max_width=100,icon=folium.Icon(color='green', icon='ok-sign')).add_to(feature_group)
    for l in manabeModel.objects.all():
        Marker(location=[l.location.y,l.location.x],popup=l.shahr,icon=folium.Icon(color='red', icon='remove-sign')).add_to(feature_group)
    # Marker(landuse,
    #       popup='Timberline Lodge').add_to(feature_group)
    
    feature_group.add_to(m)

    minimap = MiniMap(toggle_display=True)
    minimap.add_to(m)
    draw = Draw(export = False )
    MousePosition(position="bottomleft").add_to(m)
    draw.add_to(m)
 

   
    
    ostan = gpd.read_file(
    '/home/milad/Desktop/ostan.json',
    driver='GeoJSON'
    )
    
    ostangeo = folium.GeoJson(
    ostan,
    name='استان‌های ایران',
    tooltip=folium.GeoJsonTooltip(
        fields=['NAME_1'],
        localize=True)
    ).add_to(m)


    statesearch = Search(layer=ostangeo,geom_type='Polygon',
    placeholder='جستجوی استان‌های ایران',collapsed=True,search_label='NAME_1',
    weight=3).add_to(m)

    cities = gpd.read_file(
    '/home/milad/Desktop/iran.geojson',
    driver='GeoJSON'
    )
    
    citygeo = folium.GeoJson(
    cities,
    name='شهرهای ایران',
    tooltip=folium.GeoJsonTooltip(
        fields=['PLACE'],
        localize=True)
    ).add_to(m)


    statesearch = Search(layer=citygeo,geom_type='Point',
    placeholder='جستجوی شهرهای ایران',collapsed=False,search_label='NAME',
    weight=3,position='topright').add_to(m)
    folium.LayerControl().add_to(m)
    return HttpResponse(m.get_root().render())