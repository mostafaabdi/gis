from django.contrib import admin
from django.contrib.gis import admin
from .models import manabeModel

admin.site.register(manabeModel, admin.GeoModelAdmin)