from django.db import models


from django.contrib.gis.db import models
from django.contrib.gis.geos import Point

class manabeModel(models.Model):
    mahsol1= models.CharField(max_length=20)
    mahsol2= models.CharField(max_length=20)
    mahsol3= models.CharField(max_length=20)
    ostan = models.CharField(max_length=30)
    shahr= models.CharField(max_length=40)
    masahat_zamin=models.IntegerField()
    location=models.PointField()
    mpoly = models.MultiPolygonField()
